import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class OddOrPosTest
{
   // this test fails!
   @Test public void negativeOddNumbers() 
   {
      int arr[] = {-3, -2, 0, 1, 4};
      assertEquals("Negative odd numbers in array", 4, OddOrPos.oddOrPos(arr));
   }
   
   // Test que no ejecuta el codigo del fallo
   @Test public void test1() 
   {
      int arr[] = {};
      assertEquals("Empty array", 0, OddOrPos.oddOrPos(arr));
   }
   
   // test que ejecute el codigo del fallo pero no hay error de estado
   @Test public void test3() 
   {
      int arr[] = {2, 3, 5};
      assertEquals("No negatives in array", 3, OddOrPos.oddOrPos(arr));
   }
}
