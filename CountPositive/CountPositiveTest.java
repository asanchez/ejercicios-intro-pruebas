import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class CountPositiveTest
{
   // this test fails!
   @Test public void arrayContainsZeroes() 
   {
      int arr[] = {-4, 2, 0, 2};
      assertEquals("Array contains zeroes", 2, CountPositive.countPositive(arr));
   }
   
   // Test que no ejecuta el codigo del fallo
   @Test public void test1() 
   {
      int arr[] = {};
      assertEquals("Empty array", 0, CountPositive.countPositive(arr));
   }
   
   // test que ejecute el codigo del fallo pero no hay error de estado
   @Test public void test3() 
   {
      int arr[] = {2, 3, 5, -2};
      assertEquals("No zeros in array", 3, CountPositive.countPositive(arr));
   }
}
