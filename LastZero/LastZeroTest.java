import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class LastZeroTest
{
   // this test fails!
   @Test public void multipleZeroes() 
   {
      int arr[] = {0, 1, 0};
      assertEquals("Multiple zeroes: should find last one", 2, LastZero.lastZero(arr));
   }
   
   // Test que no ejecuta el codigo del fallo
   @Test public void test1() 
   {
      int arr[] = {};
      assertEquals("Not found", -1, LastZero.lastZero(arr));
   }
   
   // test que ejecute el codigo del fallo pero no hay error de estado
   @Test public void test2() 
   {
      int arr[] = {2, 3, 5, 0};
      assertEquals("Last occurence in last element", 3, LastZero.lastZero(arr));
   }
   
   // test que ejecute el codigo del fallo pero hay error de estado
   @Test public void test3() 
   {
      int arr[] = {2, 3, 0, 2};
      assertEquals("0 in between array ", 2, LastZero.lastZero(arr));
   }
}
