import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class FindLastTest
{
   // this test fails!
   @Test public void lastOccurrenceInFirstElement() 
   {
      int arr[] = {2, 3, 5};
      int y = 2;
      assertEquals("Last occurence in first element", 0, FindLast.findLast(arr, y));
   }
   
   // Test que no ejecuta el codigo del fallo
   @Test public void test1() 
   {
      int arr[] = {};
      int y = 2;
      assertEquals("Not found", -1, FindLast.findLast(arr, y));
   }
   
   // test que ejecute el codigo del fallo pero no hay error de estado
   @Test public void test2() 
   {
      int arr[] = {2, 3, 5, 2};
      int y = 2;
      assertEquals("Last occurence in last element", 3, FindLast.findLast(arr, y));
   }
   
   // test que ejecute el codigo del fallo pero hay error de estado
   @Test public void test3() 
   {
      int arr[] = {2, 3, 5, 2};
      int y = 0;
      assertEquals("Last occurence in last element", -1, FindLast.findLast(arr, y));
   }

}
